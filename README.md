## Prerequisites
```
sudo apt install python-pip
sudo pip install -U platformio
sudo usermod -a -G dialout edwin
```

## Upload
```
pio run -e huzzah -t upload
```
