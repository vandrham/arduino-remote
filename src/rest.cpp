#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

ESP8266WebServer server(80);
MDNSResponder mdns;

void _rest_setup(const char *ssid, const char *password);

void rest_setup(const char *ssid)
{
    _rest_setup(ssid, "");
}

void rest_setup(const char *ssid, const char *password)
{
    _rest_setup(ssid, password);
}

void _rest_setup(const char *ssid, const char *password)
{
    // We start by connecting to a WiFi network

    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.mode(WIFI_STA);

    if (strlen(password) == 0)
        WiFi.begin(ssid);
    else
        WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    if (mdns.begin("esp8266", WiFi.localIP()))
    {
        Serial.println("MDNS responder started");
    }

    server.on("/", []() {
        server.send(200, "text/html", "Ready");
    });

    server.begin();
    Serial.println("HTTP server started");
}

void rest_define_get(const char *route, std::function<String(void)> fn)
{
    server.on(route, [fn]() {
        server.send(200, "text/html", fn());
    });
}

void rest_loop()
{
    server.handleClient();
}