#include <ESP8266WiFi.h>

void rest_setup(const char *ssid);
void rest_setup(const char *ssid, const char *password);

void rest_loop();

void rest_define_get(const char *route, std::function<String(void)> fn);