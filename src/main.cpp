#include <stdint.h>
#include <Arduino.h>

#include <IRCommand.h>
#include <RFCommand.h>

#include <OTA.h>
#include <MQTT.h>

#include "./rest.h"

uint16_t tempUp[24] = {6550, 2525, 3300, 1650, 800, 800, 800, 1650, 1650, 850, 1650, 850, 800, 850, 800, 1650, 1650, 850, 800, 850, 800, 825, 1650, 65535};
uint16_t tempDown[18] = {6600, 2500, 3250, 1700, 800, 850, 800, 1700, 3250, 1700, 800, 850, 800, 1675, 4100, 1700, 1600, 65535};

uint16_t heatUp[20] = {6550, 2550, 3250, 1700, 750, 875, 775, 1700, 4050, 900, 750, 900, 750, 1700, 3250, 875, 750, 900, 1600, 65535};
uint16_t heatDown[22] = {6600, 2500, 3250, 1700, 800, 850, 800, 1700, 2450, 850, 800, 850, 800, 850, 800, 1700, 2450, 1700, 800, 850, 1650, 65535};

uint16_t power[22] = {6550, 2500, 3250, 1700, 800, 850, 800, 1700, 1600, 850, 750, 1700, 800, 850, 800, 1700, 1600, 850, 1600, 1700, 1600, 65535};

uint16_t white[26] = {450,850,450,850,450,850,450,400,900,400,900,425,900,400,900,400,900,850,450,850,450,850,450,400,900,15150};

RFCommand rfCommand(4);
IRCommand irCommand(5);

WiFiBase wifi("globalism");
//WiFiBase wifi("H368N0F0C9C", "34C5D4995FAD");
OTA ota(&wifi);
MQTT mqtt(&wifi, "vandrham.mine.nu", "iota", "edwin", "evdh");

bool test = false;

bool running = false;

void rest_define_switch(int code, char button, bool value) {
    String switch_cmd = String(code) + "/" + button + "/" + (value ? "true" : "false");
    String rest_url = "/switch/" + switch_cmd;
    rest_define_get(rest_url.c_str(), [code, button, value, switch_cmd]() -> String {
        rfCommand.send(code, button, value);
        mqtt.publish("home/switch/state", switch_cmd.c_str());
        return "ok";
    });
}

void setup()
{
    running = false;

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
#if HUZZAH
    pinMode(2, OUTPUT);
    digitalWrite(2, LOW);
#endif
#if WEMOS
    pinMode(D2, OUTPUT);
    digitalWrite(D2, LOW);
#endif
    rest_setup("globalism");
#if HUZZAH
    digitalWrite(LED_BUILTIN, HIGH);
#endif

    ota.setup();
    mqtt.setup();

    rest_define_get("/temp/up", []() -> String { return irCommand.send(tempUp, sizeof(tempUp)); });
    rest_define_get("/temp/down", []() -> String { return irCommand.send(tempDown, sizeof(tempDown)); });
    rest_define_get("/heat/up", []() -> String { return irCommand.send(heatUp, sizeof(heatUp)); });
    rest_define_get("/heat/down", []() -> String { return irCommand.send(heatDown, sizeof(heatDown)); });
    rest_define_get("/power", []() -> String { return irCommand.send(power, sizeof(power)); });
    
    rest_define_get("/white", []() -> String { return irCommand.send(white, sizeof(white)); });

    rest_define_switch(10, 'A', true);
    rest_define_switch(10, 'B', true);
    rest_define_switch(10, 'C', true);
    rest_define_switch(10, 'D', true);
    rest_define_switch(10, 'E', true);

    rest_define_switch(10, 'A', false);
    rest_define_switch(10, 'B', false);
    rest_define_switch(10, 'C', false);
    rest_define_switch(10, 'D', false);
    rest_define_switch(10, 'E', false);

    /*
    rest_define_get("/switch/10/B/true", []() -> String { rfCommand.send(10, 'B', true); return "ok"; });
    rest_define_get("/switch/10/B/false", []() -> String { rfCommand.send(10, 'B', false); return "ok"; });
    rest_define_get("/switch/20/B/true", []() -> String { rfCommand.send(20, 'B', true); return "ok"; });
    rest_define_get("/switch/20/B/false", []() -> String { rfCommand.send(20, 'B', false); return "ok"; });
    */
    /*
    mqtt.subscribe("home/stove/temp/up", [](String message) -> void { irCommand.send(tempUp, sizeof(tempUp)); });
    delay(300);
    mqtt.subscribe("home/stove/temp/down", [](String message) -> void { irCommand.send(tempDown, sizeof(tempDown)); });
    delay(300);
    mqtt.subscribe("home/stove/heat/up", [](String message) -> void { irCommand.send(heatUp, sizeof(heatUp)); });
    delay(300);
    mqtt.subscribe("home/stove/heat/down", [](String message) -> void { irCommand.send(heatDown, sizeof(heatDown)); });
    delay(300);
    mqtt.subscribe("home/stove/power", [](String message) -> void { irCommand.send(power, sizeof(power)); });
    delay(300);

    mqtt.subscribe("home/kitchen/light/on", [](String message) -> void { rfCommand.send(10, 'B', true); });
    delay(300);
    mqtt.subscribe("home/kitchen/light/off", [](String message) -> void { rfCommand.send(10, 'B', false); });
    delay(300);
    mqtt.subscribe("home/bedroom/amp/on", [](String message) -> void { rfCommand.send(20, 'B', true); });
    delay(300);
    mqtt.subscribe("home/bedroom/amp/off", [](String message) -> void { rfCommand.send(20, 'B', false); });
    */
}

void loop()
{
    if (!running)
    {
        running = true;
        digitalWrite(LED_BUILTIN, HIGH); // red light low=on high=off
#if HUZZAH
        digitalWrite(2, HIGH); // blue light low=on high=off
#endif
    }

    ota.loop();
    mqtt.loop();

    rest_loop();
}